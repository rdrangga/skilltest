﻿using FrontEndTest.Helpers;
using FrontEndTest.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Diagnostics;

namespace FrontEndTest.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> GetDataLocation()
        {
            LocationModel locationModel = new LocationModel();
            APICaller api = new APICaller();
            JObject res = new JObject();

            try
            {
                Task<JObject> task = Task.Run<JObject>(async () => await api.API_GET_LOCATION());
                res = task.Result;

                
            }
            catch (Exception)
            {

                throw;
            }

            return Json(res);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}