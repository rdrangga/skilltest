﻿using FrontEndTest.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Text;

namespace FrontEndTest.Helpers
{
    public class APICaller
    {
        public string apiURL()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: false);

            IConfiguration config = builder.Build();

            return config.GetValue<string>("ConnectionStrings:APIUrl");
        }

        private static string SAVE_BPKB = "SaveBpkb/Data";
        private static string GET_LOCATION = "SaveBpkb/Data";

        public async Task<JObject> API_SAVE_BPKB(BpkbModel input)
        {

            JObject resz = new JObject();

            try
            {
                var stringContent = JsonConvert.SerializeObject(input);
                var contentCharging = new StringContent(stringContent, Encoding.UTF8, "application/json");
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

                using (var client = new HttpClient())
                {
                    var response = await client.PostAsync(apiURL() + SAVE_BPKB, contentCharging);
                    string result = await response.Content.ReadAsStringAsync();
                    resz = JsonConvert.DeserializeObject<JObject>(result);
                }

            }
            catch (Exception e)
            {
                string errMessage = e.InnerException != null ? e.InnerException.Message : e.Message;
            }
            return resz;
        }

        public async Task<JObject> API_GET_LOCATION()
        {

            JObject resz = new JObject();
            //ChargeGetData cgd = new ChargeGetData();
            //cgd.ProposalGeneralId = ProposalGeneralId;
            try
            {
                //var stringContent = JsonConvert.SerializeObject(input);
               // var contentCharging = new StringContent(stringContent, Encoding.UTF8, "application/json");
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(string.Format(apiURL() + GET_LOCATION));
                    string result = await response.Content.ReadAsStringAsync();
                    resz = JsonConvert.DeserializeObject<JObject>(result);
                }

            }
            catch (Exception e)
            {
                string errMessage = e.InnerException != null ? e.InnerException.Message : e.Message;
            }
            //data = JObject.FromObject(resz);
            return resz;
        }
    }
}
