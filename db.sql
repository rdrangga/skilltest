USE [master]
GO
/****** Object:  Database [SkillTest]    Script Date: 29/11/2022 15:09:10 ******/
CREATE DATABASE [SkillTest]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SkillTest', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER10\MSSQL\DATA\SkillTest.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SkillTest_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER10\MSSQL\DATA\SkillTest_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SkillTest].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SkillTest] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SkillTest] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SkillTest] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SkillTest] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SkillTest] SET ARITHABORT OFF 
GO
ALTER DATABASE [SkillTest] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SkillTest] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SkillTest] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SkillTest] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SkillTest] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SkillTest] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SkillTest] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SkillTest] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SkillTest] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SkillTest] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SkillTest] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SkillTest] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SkillTest] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SkillTest] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SkillTest] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SkillTest] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SkillTest] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SkillTest] SET RECOVERY FULL 
GO
ALTER DATABASE [SkillTest] SET  MULTI_USER 
GO
ALTER DATABASE [SkillTest] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SkillTest] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SkillTest] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SkillTest] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SkillTest] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SkillTest', N'ON'
GO
ALTER DATABASE [SkillTest] SET QUERY_STORE = OFF
GO
USE [SkillTest]
GO
/****** Object:  Table [dbo].[ms_storage_location]    Script Date: 29/11/2022 15:09:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ms_storage_location](
	[location_id] [varchar](10) NOT NULL,
	[location_name] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[location_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tr_bpkb]    Script Date: 29/11/2022 15:09:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tr_bpkb](
	[aggrement_number] [varchar](100) NOT NULL,
	[bpkb_no] [varchar](100) NOT NULL,
	[branch_id] [varchar](100) NOT NULL,
	[bpkb_date] [datetime] NOT NULL,
	[faktur_no] [varchar](100) NOT NULL,
	[faktur_date] [datetime] NOT NULL,
	[location_id] [varchar](10) NOT NULL,
	[police_no] [varchar](20) NOT NULL,
	[bpkb_date_in] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[aggrement_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tr_bpkb]  WITH CHECK ADD  CONSTRAINT [FK_LocationId] FOREIGN KEY([location_id])
REFERENCES [dbo].[ms_storage_location] ([location_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tr_bpkb] CHECK CONSTRAINT [FK_LocationId]
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertBpkb]    Script Date: 29/11/2022 15:09:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertBpkb] 
	@msg VARCHAR(MAX) = '' OUTPUT,
	@aggrement_number VARCHAR(100),
	@bpkb_no VARCHAR(100),
	@branch_id VARCHAR(100),
	@bpkb_date DATETIME,
	@faktur_no VARCHAR(100),
	@faktur_date DATETIME,
	@location_id VARCHAR(10),
	@police_no VARCHAR(20),
	@bpkb_date_in DATETIME
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION proses_insert


	INSERT INTO [dbo].[tr_bpkb]
           ([aggrement_number]
           ,[bpkb_no]
           ,[branch_id]
           ,[bpkb_date]
           ,[faktur_no]
           ,[faktur_date]
           ,[location_id]
           ,[police_no]
           ,[bpkb_date_in])
     VALUES
           (@aggrement_number
           ,@bpkb_no
           ,@branch_id
           ,@bpkb_date
           ,@faktur_no
           ,@faktur_date
           ,@location_id
           ,@police_no
           ,@bpkb_date)

	COMMIT TRAN proses_insert;

        SET @msg = 'Successfuly Insert';
		SELECT @msg msg;
	END TRY
    BEGIN CATCH
        SET @msg = 'Failed Insert Data';
		SELECT @msg msg;

        ROLLBACK TRAN proses_insert;
    END CATCH;
END;
GO
USE [master]
GO
ALTER DATABASE [SkillTest] SET  READ_WRITE 
GO
