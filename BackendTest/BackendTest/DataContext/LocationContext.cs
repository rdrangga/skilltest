﻿using Microsoft.EntityFrameworkCore;
using Skilltest.Models;

namespace Skilltest.DataContext
{
    public class LocationContext : DbContext
    {
        public LocationContext(DbContextOptions<LocationContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LocationModel>().HasNoKey().ToView(null);
        }

        public IQueryable<LocationModel> GetMasterLocation() =>
    Set<LocationModel>()
    .FromSqlRaw(
        $"SELECT * FROM ms_storage_location"
    );
    }
}
