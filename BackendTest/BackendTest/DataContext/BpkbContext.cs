﻿using Microsoft.EntityFrameworkCore;
using Skilltest.Models;

namespace Skilltest.DataContext
{
    public class BpkbContext : DbContext
    {
        public BpkbContext(DbContextOptions<BpkbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ResponseSP>().HasNoKey().ToView(null);
        }

        public IEnumerable<ResponseSP> SaveDataBpkb(BpkbModel bpkb) =>
           Set<ResponseSP>()
           .FromSqlRaw(
               $"EXEC sp_InsertBpkb  '','{bpkb.aggrement_number}','{bpkb.bpkb_no}','{bpkb.branch_id}','{bpkb.bpkb_date}','{bpkb.faktur_no}','{bpkb.faktur_date}','{bpkb.location_id}','{bpkb.police_no}','{bpkb.bpkb_date_in}'")
           .AsEnumerable();
    }
}
