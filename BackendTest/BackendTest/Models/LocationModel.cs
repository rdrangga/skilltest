﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Skilltest.Models
{
    public class LocationModel
    {
        [Key]
        public string? location_id { get; set; }
        public string? location_name { get; set; }
    }
}
