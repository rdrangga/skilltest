﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Skilltest.DataContext;
using Skilltest.Models;

namespace Skilltest.Controllers
{
    [ApiController]
    public class MCFController : ControllerBase
    {
        private readonly BpkbContext _dbContexts;
        private readonly LocationContext _dbContextsLoc;

        public MCFController(BpkbContext dbContext, LocationContext dbContextLoc)
        {
            _dbContexts = dbContext;
            _dbContextsLoc = dbContextLoc;
        }

        [HttpGet]
        [Route("Master/Location")]
        public async Task<ActionResult<IEnumerable<LocationModel>>> GetLocation()
        {
            if (_dbContextsLoc.GetMasterLocation() == null)
            {
                return NotFound();
            }
            return await _dbContextsLoc.GetMasterLocation().ToListAsync();
        }


        [HttpPost]
        [Route("Save/Bpkb")]
        public async Task<IActionResult> SaveBpkb([FromBody] BpkbModel bpkb)
        {
            ResponseSP res = new ResponseSP();
            try
            {
                if (bpkb == null)
                {
                    return NotFound();
                }
                else
                {
                    await Task.Run(() =>
                    {
                        res = _dbContexts.SaveDataBpkb(bpkb).FirstOrDefault();
                    });

                    return Ok(res);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
                throw;
            }
        }
    }
}
